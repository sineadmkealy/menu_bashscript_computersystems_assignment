#!/bin/bash
# STUDENT NAME: Sinead Mary Kealy
# COURSE TITLE: H.Dip.Sc Computer Science, 2016 
# STUDENT NO.: 20073382
#-------------------------------------------------------------------------------------------------------
# A script to add new customers by updating the CustomerDetails file with customer details 
# ---------------------------------------------------------------------------------------------------
# BACKGROUND PAGE SETUP AND DISPLAY INSTRUCTIONS

# This while loop will allow thIS page to execute, then navigate to any of the following: 
# the beginning of this Menu age, to further menu selections or to exit the programme
end=0 #start of while loop
while [ $end -eq 0 ]
do 
	# clears this screen each time a User enters/returns to this Menu page
	clear

	# defines the text colour to green with black background (where no separate background passed in)
	green='\e[0;32m' 
	endColor='\e[0m'

	# a separate cyan text colour scheme is created for selected use in the file
	magenta='\e[0;36m' 
	endColor='\e[0m'

	# defines the menu frame function to be used to border the Menu text
	function frame
	{
	echo -e "${green}***********************************************************" 
	}

	# defines a general font cyan colour to be used in specifric locations
	function gen_font
	{
	echo -e "${magenta}"
	}

	# generally used where selections required
	function emphasis_font
	{
	echo -e "${green}"
	}

	#bold text function, not actually invoked in end - too difficult to pick out text variable
	# could get this to work as a function, done manually below, as required
	function highlight
	{
	echo -e "\e[1;32m {$text} \n${endColor}"
	}
#_____________________________________________________________________________________________
# THE ADD CUSTOMER PAGE FUNCTIONS

# Confirms the logged-in User in the 'Add Customer' page
	echo -e "${green}Logged-in User: \e[1;32m $USER \n${endColor}" 
	# \e[1;32;40m : \e[attribute code (bold);text color code (green);background color code(black)

	#to display the Menu framing lines, calls the highlight function defined before
	frame Menu 

	# Menu title in bold text
	echo -e "\e[1;32m          ***UPDATE / ADD NEW CUSTOMER DETAILS**                \n${endColor}"

	# calls cyan text function
	gen_font 

	echo Please enter the following customer details: 
	echo # space line
	echo " * Email address * Name * Alias * Address * Telephone number"
	echo
	# calls frame function and reverts text to green colour
	frame Menu 

# VALID EMAIL CHECK AND CORRECT DETAILS CHECK 
	# Outer outer While Loop to contain 2 checks:
	# 1 A valid email address entered (While Loop)
	# 2 That customer happy to save this address (If-else statement)
	end=0 #start of outer while loop
	while [ $end -eq 0 ]
	do 
		# While loop to firstly, check a valid email address entered
		echo
		echo 'Email: (please ensure a valid email address in the format xxxx@xxx.xx)' 
		read -p email # to prompt as writing
		#while loop to check for a valid email address
		# PROBLEM HERE IT ALWAYS FAILS MATCH THE F1RST TIME????
		# while [[ $email != *?@?*.?* ]]; 
		while [[ $email != *?"@"?*"."?* ]];
		#while email address does not contain the @ symbol and .(com) syntax
		do
			echo Invalid email address entered
			echo Please enter a new email address
			read -p 'Email: ' email
	done
	# If-else statement to check, where correct format number entered
		# that customer satisfied that correct details entered
	echo "Valid email address: $email  entered, thankyou"
	echo
	echo "Do you wish to save this email(y/n)?"
		read response	
		# response not case-senstive - can achieve it on the read statement (read -i) but 
		# unable to figure for the if-else match syntax
		if [ $response = "y" ]
		then
			echo "Email address saved. Thankyou."
			end=1 #exit the outer loop at this stage id customer happy with details
		else
			echo Please re-enter the email address
		# end of if-else check that customer satisfied with details entered
 		fi
	done
	# end of outer While Loop chjeck that 2 conditions satisfied
	echo "Email address added to Customer Details. Thankyou." 
	# Don't need to print entire address again
	frame

# CORRECT FIRST-NAME CHECK
	echo
	read -p 'First Name: ' firstname
	echo "You entered '$firstname'. Is this correct (y/n)?"
	read response
	# read -i response	# response not case-senstive [COULDN'T GET THIS TO WORK]
	# while loop to allow customer satisfaction that correct details entered
		while [[  $response != "y" ]];
		do
			echo Please enter the customer name again
			read name
			echo "You entered '$firstname'. Is this correct (y/n)?"
			read response
		done	
		echo "'$firstname' added to Customer Details. Thankyou."
		frame

# CORRECT SURNAME CHECK
	echo
	read -p 'Surname: ' surname
	echo "You entered '$surname'. Is this correct (y/n)?"
	read response
	# read -i response	# response not case-senstive [COULDN'T GET THIS TO WORK]
	# while loop to allow customer satisfaction that correct details entered
		while [[  $response != "y" ]];
		do
			echo Please enter the customer name again
			read name
			echo "You entered '$surname'. Is this correct (y/n)?"
			read response
		done	
		echo "'$surname' added to Customer Details. Thankyou."
		frame

# CORRECT ALIAS CHECK
	echo
	read -p 'Alias : ' alias
	echo "You entered '$alias'. Is this correct (y/n)?"
	# read case insenstive response for y/n
	read response	
	# while loop to allow customer satisfaction that correct details entered
		while [[ $response != "y" ]];
		do
			echo Please enter the customer alias again
			read alias
			echo "You entered '$alias'. Is this correct (y/n)?"
			read response
		done	
		echo "'$alias' added to Customer Details. Thankyou."
		frame

# CORRECT ADDRESS CHECK
	echo
	read -p 'Address: ' address
	echo "You entered '$address'. Is this correct (y/n)?"
	read response	
	# while loop to allow customer satisfaction that correct details entered
		while [[  $response != "y" ]];
		do
			echo Please enter the customer address again
			read address
			echo "You entered '$address'. Is this correct (y/n)?"
			read response
		done	
		echo "Address added to Customer Details. Thankyou." 
		# Don't need to print entire address again
		frame

# VALID PHONE NUMBER (DIGITS ONLY) AND CORRECT NUMBER CHECK 
	# Outer outer While Loop to contain 2 checks:
	# 1 A valid number entered (While Loop)
	# 2 That customer happy to save this number (If-else statement)
	end=0 #start of while loop
	while [ $end -eq 0 ]
	do 
		echo
		echo "Please ensure a valid telephone number comprising numbers only"
		echo "Include country code in the format 00353869993999 (no spaces)"
		echo
		read -p 'Telephone No: ' phone
		# While loop to firstly, check a valid number entered
		# we don't boteher to check for number of digits due to possible permutations
		# while [[ "$phone" != ^[0-9]+$ ]] && [ "$phone" -ge 1 -a "$phone" -le 32 ]; 
		while ! [[ "$phone" =~ ^[0-9]+$ ]] # while phone noy in numeric format
		do
  		echo "Invalid input (not numeric): $phone"
 			echo "Please enter a new number in the correct format"
  		read -p 'Telephone No: ' phone
  	# end of inner While loop check for valid number	
		done
		# If-else statement to check, where correct format number entered
		# that customer satisfied that correct details entered
		echo "Valid telephone number entered, thankyou"
		echo $phone			
		echo
		echo "Do you wish to save this number(y/n)?"
		read response	# response not case-senstive	
		if [ $response = "y" ]
		then
			echo "Phone number saved. Thankyou."
			end=1 #exit the outer loop at this stage if customer happy with details
		else
			echo Please re-enter your number	
		# end of if-else check that customer satisfied with details entered
 		fi
	done
	# end of outer While Loop check that 2 conditions satisfied
	echo "Telephone number added to Customer Details. Thankyou." 
	# Don't need to print entire address again
	frame

	echo
	# To highlight save details confirmation
	echo -e "\e[1;32mThankyou. Please check your information submitted \n${endColor}"
	
	gen_font # calls cyan text function
	echo -e "Email: " $email'\n'"Name: " $name'\n'"Alias: " $alias'\n'"Address: " $address'\n'"Telephone number: " $phone'\n'
	sleep 3

	emphasis_font # reverts to green text where selection required
	echo "Are these details correct(y/n)?"
	read response 
	if [ $response == "y" ] 
	then
		#echo "$*" >> CustDetails.sh # to add all the arguments passed by User to the CustDetails file
		echo $email 	$name	$alias $address $phone >> CustDetails.tsv 
		sort -o CustDetails.tsv CustDetails.tsv # to sort the submitted customer details alphaBetically
		echo "Details added to file"
	else
		echo "Details incorrect, please try again"		
	fi
	echo
	frame

	# This will direct customer to begin process of adding customer details again, if neccessary
	# outputs a mixed text font layout per line, hihglighting the imnportance of the number selection
	echo
	echo Please choose your next option:
	echo
	echo -e "${green}  1. ${endColor}" "${magenta}Add another Customer${endColor}" 
	echo -e "${green}  2. ${endColor}" "${magenta}Return to the Main Menu${endColor}" 
	echo -e "${green}  3. ${endColor}" "${magenta}Exit${endColor}" 
	echo
	frame
	sleep 2 # wait 2 seconds

# calls cyan text function
	gen_font 
	echo Enter Number now
	read selection

# ---------------------------------------------------------------------------------------------------
	# NAVIGATION INSTRUCTIONS TO THE SHELL

	# uses case statement for selection
	# note 'exit' command after each navigation option 
	# this ensures this script instance closes after exit 
	case $selection in 
	#1) end=0;; will loop automatically to beginning this file
	2) ./Menu.sh; exit;; # exits this page and navigates back to Menu page
	3) end=1;; #exits the program
	#*) echo invalid input; exit;; # triggers the user to input another selection
	# causes error with selection of 1 where 1) not explicitly stated and 
	# 1) is instead interpreted as wild card
	esac

done
