#!/bin/bash
# STUDENT NAME: Sinead Mary Kealy
# COURSE TITLE: H.Dip.Sc Computer Science, 2016 
# STUDENT NO.: 20073382
#-------------------------------------------------------------------------------------------------------
# This 'Address book' project comprises a "menu driven" shell program to allow the user to interactively 
# carry out the following:
# add customer information
# remove customer information
# search and display customer information
# email a customer

#This Menu page acts as a central navigation page from which users select their requirement and 
# nare directed to the selected menu function page, and
# to which they navigate back to, to select further function options on the Address Book
# Users may exit the program from any page
# ---------------------------------------------------------------------------------------------------
# BACKGROUND PAGE SETUP AND DISPLAY INSTRUCTIONS

# This while loop will allow thIS page to execute, then navigate to any of the following: 
# the beginning of this Menu age, to further menu selections or to exit the programme
end=0 #start of while loop
while [ $end -eq 0 ]
do 

	# clears this screen each time a User enters/returns to this Menu page
	clear

	# defines the text colour to green with black background (where no separate background passed in)
	green='\e[0;32m' 
	endColor='\e[0m'

	# a separate cyan text colour scheme is created for selected use in the file
	magenta='\e[0;36m' 
	endColor='\e[0m'

	# defines the menu frame function to be used to border the Menu text
	function frame
	{
	echo -e "${green}***********************************************************" 
	}

	# defines a general font cyan colour to be used in specifric locations
	function gen_font
	{
	echo -e "${magenta}"
	}

	#bold text function, not actually invoked in end - too difficult to pick out text variable
	function highlight
	{
	echo -e "\e[1;32m {text} \n${endColor}"
	}
	#_____________________________________________________________________________________________
# THE MENU PAGE FUNCTIONS

	# Welcomes the User to the Menu
	echo -e "${green}Welcome \e[1;32;41m $USER \n${endColor}" 
	# \e[1;32;40m : \e[attribute code (bold);text color code (green);background color code(black)

	#to display the Menu framing lines, calls the highlight function defined before
	frame Menu 

	# Menu title in bold text
	echo -e "\e[1;32m                    ***MENU***                \n${endColor}"

	# calls cyan text function
	gen_font 

	echo Please choose one of the following number selections:
	echo # create space line
	# outputs a mixed text font layout per line, hihglighting the imnportance of the number selection
	echo -e "${green}  1.${endColor}" "${magenta}Add a new Customer${endColor}" 
	echo -e "${green}  2.${endColor}" "${magenta}Remove an existing Customer${endColor}" 
	echo -e "${green}  3.${endColor}" "${magenta}Search for a Customer${endColor}" 
	echo -e "${green}  4.${endColor}" "${magenta}E-mail a Customer${endColor}" 
	echo -e "${green}  5.${endColor}" "${magenta}Exit${endColor}" 
	echo

	# calls frame function and reverts text to green colour
	frame Menu 

	# Uset to enter number selection to allow further Menu navigation
	echo
	echo Enter Number now:

	read selection

# ---------------------------------------------------------------------------------------------------
	# NAVIGATION INSTRUCTIONS TO THE SHELL

	# uses case statement for selection
	# note 'exit' command after each navigation option 
	# this ensures this script instance closes after exit 
	# so that several menu pages are not running in background
	case $selection in 
		1) ./AddCust.sh; exit;; # execute the Add Customer script, and close this page
		2) ./RemCust.sh; exit;; # execute the Remove Customer script, and close this page
		3) ./SearchCust.sh; exit;; # execute the Find Customer script, and close this page
		4) ./Email.sh; exit;; # execute the Email script, and close this page
		5) end=1;; #end of the while loop, exits the file
		*) echo invalid input; exit;; # triggers the user to input another selection
	esac #closes case statement

#end of while loop
done


