#!/bin/bash
# STUDENT NAME: Sinead Mary Kealy
# COURSE TITLE: H.Dip.Sc Computer Science, 2016 
# STUDENT NO.: 20073382
#-------------------------------------------------------------------------------------------------------
# A script to allow the user to email any or all customers listed in the
# CustDetails address book
# Searches for email address by customer name or typing 'ALL' to send to all customers
# ---------------------------------------------------------------------------------------------------
# BACKGROUND PAGE SETUP AND DISPLAY INSTRUCTIONS

# This while loop will allow thIS page to execute, then navigate to any of the following: 
# the beginning of this Menu age, to further menu selections or to exit the programme
end=0 #start of while loop
while [ $end -eq 0 ]
do 
	# clears this screen each time a User enters/returns to this Menu page
	clear

	# defines the text colour to green with black background (where no separate background passed in)
	green='\e[0;32m' 
	endColor='\e[0m'

	# a separate cyan text colour scheme is created for selected use in the file
	magenta='\e[0;36m' 
	endColor='\e[0m'

	# defines the menu frame function to be used to border the Menu text
	function frame
	{
	echo -e "${green}******************************************************************" 
	}

	# defines a general font cyan colour to be used in specifric locations
	function gen_font
	{
	echo -e "${magenta}"
	}

	# generally used where selections required
	function emphasis_font
	{
	echo -e "${green}"
	}

	#bold text function, not actually invoked in end - too difficult to pick out text variable
	# could get this to work as a function, done manually below, as required
	function highlight
	{
	echo -e "\e[1;32m {$text} \n${endColor}"
	}
	#_____________________________________________________________________________________________
# THE EMAIL CUSTOMER PAGE FUNCTIONS

# Confirms the logged-in User in the 'Add Customer' page
	echo -e "${green}Logged-in User: \e[1;32m $USER \n${endColor}" 
	# \e[1;32;40m : \e[attribute code (bold);text color code (green);background color code(black)

	#to display the Menu framing lines, calls the highlight function defined before
	frame Menu 
	# Menu title in bold text
	echo -e "\e[1;32m          ***EMAIL CUSTOMER(S)***            \n${endColor}"
	# calls cyan text function
	gen_font 
	echo "Do you wish to email to an individual Customer or to ALL customers?"
	echo
	echo "Enter the customer name" 
	echo "Alternatively, type 'ALL' to send to all customers"
	echo
	frame Menu 

read response # cannot get case sensitivity to work in loops tha follow
# information to obtain individual email addresses

# OPTION 1: If email to be sent to ALL customers in the Address Book
# Outer If-Else statment
if [ $response == "ALL" ]
then
	#outputs the contents of the file right onto the terminal
	emailAll=`cat CustDetails.tsv | awk '{print $1}'` 
	# Confirm that User wishes to send to all customers
	echo "You entered email to be sent to ALL customers. Is this correct (y/n)?"
	read confirmation
	# Inner NESTED If-else confirmation check
	if [ $confirmation == "y" ] 
	then
		echo
		frame
		echo
		echo Message for ALL customers #: $emailAll
		read -p 'Subject: ' subject
		echo
		frame
		echo
		echo Enter any cc addresses then press return
		echo
		echo Please type your email message
		echo
		echo When complete, press CONTRL+D on new line to end 
		echo
		frame
		echo
		mail -s "Message for All customers, Subject $subject" $emailAll
		echo SEND
		echo
		frame
		echo
		echo Your mail has been successfully sent
		echo
		frame
	else
		echo No email sent
	fi
 
# OPTION 2: If email to be sent to individual customer
# Checks where 2 or more customer of same name, to extract the individual email address
# Was unable to run this successfully


# count_match=`grep -ci $response	CustDetails.tsv`
# emailaddr=`grep -i $response CustDetails.tsv | awk '{print $1}'`

# elif [ $count_match -ge 2 ] #greater than or equal to 2
# then 
	# frame
	# echo "$count_match customers match the name submitted"
	# echo
	# echo "Customer email address is : "
	# echo $emailaddr # to separate the results on new line
	# echo

	#to create a list, extracting a list of email addresses from column 1 in the CustDetails.sh file
	# echo
	# echo "Please select the line no of the correct email address"
	# read -p 'Line no.: ' linenumber
	# echo

	# I cannot see any examples of how to execute this online and am perplexed as to how classmates
	# have the skillset to achieve it, unless from prior coding experience.  
	# It did not feature on Ryan tutorials, tuition etc.
	# I will have to leave it for now, unfortunately.  My attempt commented out.
	# Lesser functional working section included below.

	# finalemailaddr=`grep -i $linenumber CustDetails.tsv | awk 'NR == num_line' file`
	# finalemailaddr=`grep -i $linenumber CustDetails.tsv | awk '{print $1}'`
 	# echo
 	# frame
 	# echo
	# echo "Message for $response" #: $emailAll
	# read -p 'Subject: ' subject
	# echo
	# frame
	# echo
	# echo Enter any cc addresses then press return
	# echo
	# echo Please type your email message
	# echo
	# echo When complete, press CONTRL+D on new line to end 
	# echo
	# frame
	# echo
	# mail -s "Message for $response, Subject $subject" $finalemailaddr
	# echo SEND
	# echo
	# frame
	# echo
	# echo Your mail has been successfully sent
	# echo
	# frame

# OPTION 2: If email to be sent to individual customer
# simplified where only one customer name exists
# This works for FirstName entered only, not SurName
# No time to explore further

else
echo
	frame
	emailaddr=`grep -i $response CustDetails.tsv | awk '{print $1}'`
	#to create a list, extracting a list of email addresses from column 1 in the CustDetails.sh file
		echo
		echo "Email to be sent to $emailaddr. Is this correct (y/n)?"
		read confirmation
		# Inner NESTED If-else confirmation check
		if [ $confirmation == "y" ] 
		then
			echo
			echo "Message for $response"
			echo
			read -p 'Subject: ' subject
			echo
			frame
			echo
			echo Enter any cc addresses then press return
			echo
			echo Please type your email message
			echo
			echo When complete, press CONTRL+D on new line to end 
			echo
			frame
			echo
			mail -s "Message for response, Subject $subject" $emailaddr
			echo SEND
			echo
			frame
			echo
			echo Your mail has been successfully sent
			echo
			frame
		else
			echo No email sent
		fi
# End of outer If-else statement (choice of all or individual email address)
fi
echo
	frame

	# This will direct customer to begin process of adding customer details again, if neccessary
	# outputs a mixed text font layout per line, hihglighting the imnportance of the numbe
echo
	echo Please choose your next option:
	echo
	echo -e "${green}  1. ${endColor}" "${magenta}Send another email${endColor}" 
	echo -e "${green}  2. ${endColor}" "${magenta}Return to the Main Menu${endColor}" 
	echo -e "${green}  3. ${endColor}" "${magenta}Exit${endColor}" 
	echo
	frame
	sleep 2 # wait 2 seconds

# calls cyan text function
	gen_font 
	echo Enter Number now
	read selection

# ---------------------------------------------------------------------------------------------------
	# NAVIGATION INSTRUCTIONS TO THE SHELL

	# uses case statement for selection
	# note 'exit' command after each navigation option 
	# this ensures this script instance closes after exit 
	case $selection in 
	#1) end=0;; will loop automatically to beginning this file
	2) ./Menu.sh; exit;; # exits this page and navigates back to Menu page
	3) end=1;; #exits the program
	#*) echo invalid input; exit;; # triggers the user to input another selection
	# causes error with selection of 1 where 1) not explicitly stated and 
	# 1) is instead interpreted as wild card
	esac

done

