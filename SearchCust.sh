#!/bin/bash
# STUDENT NAME: Sinead Mary Kealy
# COURSE TITLE: H.Dip.Sc Computer Science, 2016 
# STUDENT NO.: 20073382
#-------------------------------------------------------------------------------------------------------
# A file to alow the User to search an existing customer
# based on any of the customer's personal information, including:
# Email address, Name, Alias/Nickname, Address, Telephone No.
# ---------------------------------------------------------------------------------------------------
# BACKGROUND PAGE SETUP AND DISPLAY INSTRUCTIONS

# This while loop will allow thIS page to execute, then navigate to any of the following: 
# the beginning of this Menu age, to further menu selections or to exit the programme
end=0 #start of while loop
while [ $end -eq 0 ]
do 
	# clears this screen each time a User enters/returns to this Menu page
	clear

	# defines the text colour to green with black background (where no separate background passed in)
	green='\e[0;32m' 
	endColor='\e[0m'

	# a separate cyan text colour scheme is created for selected use in the file
	magenta='\e[0;36m' 
	endColor='\e[0m'

	# defines the menu frame function to be used to border the Menu text
	function frame
	{
	echo -e "${green}***********************************************************" 
	}

	# defines a general font cyan colour to be used in specifric locations
	function gen_font
	{
	echo -e "${magenta}"
	}

	# generally used where selections required
	function emphasis_font
	{
	echo -e "${green}"
	}

	#bold text function, not actually invoked in end - too difficult to pick out text variable
	# could get this to work as a function, done manually below, as required
	function highlight
	{
	echo -e "\e[1;32m {$text} \n${endColor}"
	}
#_____________________________________________________________________________________________
# THE SEARCH CUSTOMER PAGE FUNCTIONS


# Confirms the logged-in User in the 'Add Customer' page
	echo -e "${green}Logged-in User: \e[1;32m $USER \n${endColor}" 
	# \e[1;32;40m : \e[attribute code (bold);text color code (green);background color code(black)

	#to display the Menu framing lines, calls the highlight function defined before
	frame Menu 

	# Menu title in bold text
	echo -e "\e[1;32m        ***SEARCH FOR CUSTOMER DETAILS**              \n${endColor}"

	# calls cyan text function
	gen_font 

	echo "Please search for customer by ANY of the following search terms :"
	echo # space line
	echo " * Email address * Name * Alias * Address * Telephone number"
	echo

	# calls frame function and reverts text to green colour
	frame Menu 
	echo
	echo "Please enter your search details eg. john, nolan, turncoats hall, Mallow etc.."
	echo

	# Inner While loop if customer has not entered correct search details 
	# ABANDONED THIS OPTION IN THE END, CAUSED DIFFICULTY WITH CASE STATEMENT FOR 
	# MAIN PAGE NAVIGATION
	# begins process again
	#end=0 #start of while loop
	#while [ $end -eq 0 ]
	#do 
		read -p 'Search term: ' searchterm
		echo
		echo "You entered the searchterm '$searchterm'. Is this correct (y/n)?"
		read response 

		num_match=`grep -ci $searchterm CustDetails.tsv` # the number of case-insenstive matches
		match=`grep -i $searchterm CustDetails.tsv`  #case insenstive match

		# Outer Else-If statement for customer to confirm search details corect
		if [ $response == "y" ] 
		then
		# if one or more matches exist
			if [[ $match ]] 
			then
				frame
				echo
				echo "Number of matches to your query: $num_match no."
				echo
				echo -e "\e[1;32mCustomer details as follows : \n${endColor}"
				gen_font 
				echo "$match\n" # to separate the results on new line
				echo
				frame
				end=1 #exit the outer loop at this stage if search details provide results())
			else
				echo "No customer name exists to match your query"
				echo
				frame
			fi
		else
			echo Incorrect details entered
			echo
			frame
		fi	
	# Abandoned this While Loop, interfered with main menu :
	# If results not found, it loops around again to search again
	# If results are found it continues down to end menu

	echo
	echo Please choose your next option:
	echo
	echo -e "${green}  1. ${endColor}" "${magenta}Find another Customer${endColor}" 
	echo -e "${green}  2. ${endColor}" "${magenta}Return to the Main Menu${endColor}" 
	echo -e "${green}  3. ${endColor}" "${magenta}Exit${endColor}" 
	echo
	frame
	sleep 2 # wait 2 seconds

# calls cyan text function
	gen_font 
	echo Enter Number now
	read selection

# ---------------------------------------------------------------------------------------------------
	# NAVIGATION INSTRUCTIONS TO THE SHELL

	# uses case statement for selection
	# note 'exit' command after each navigation option 
	# this ensures this script instance closes after exit 
	case $selection in 
	# 1) end=0;; will loop automatically to beginning this file
	2) ./Menu.sh; exit;; # back to Menu # back to Menu
	3) end=1;;
	#*) echo invalid input; exit;; # triggers the user to input another selection
	# causes error with selection of 1 where 1) not explicitly stated and 
	# 1) is instead interpreted as wild card
	esac

 
done