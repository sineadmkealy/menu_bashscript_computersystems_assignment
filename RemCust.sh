#!/bin/bash
# STUDENT NAME: Sinead Mary Kealy
# COURSE TITLE: H.Dip.Sc Computer Science, 2016 
# STUDENT NO.: 20073382
#-------------------------------------------------------------------------------------------------------
# A script to remove all of a customer's information from the CustDetails file
# based initially on the customer's name (firstname and surname) 
# if more than one match exists, a further search by alias check is undertaken
# Note specific columns format in CustomerDetails file:
# $1:Email address, $2:FirstName, $3:SurName, $4:Alias(Nickname), $5:Address, $6:Telephone number
# ---------------------------------------------------------------------------------------------------
# BACKGROUND PAGE SETUP AND DISPLAY INSTRUCTIONS

# This while loop will allow this page to execute, then navigate to any of the following: 
# the beginning of this Menu age, to further menu selections or to exit the programme
end=0 #start of while loop
while [ $end -eq 0 ]
do 
	# clears this screen each time a User enters/returns to this Menu page
	clear

	# defines the text colour to green with black background (where no separate background passed in)
	green='\e[0;32m' 
	endColor='\e[0m'

	# a separate cyan text colour scheme is created for selected use in the file
	magenta='\e[0;36m' 
	endColor='\e[0m'

	# defines the menu frame function to be used to border the Menu text
	function frame
	{
	echo -e "${green}***********************************************************" 
	}

	# defines a general font cyan colour to be used in specifric locations
	function gen_font
	{
	echo -e "${magenta}"
	}

	# generally used where selections required
	function emphasis_font
	{
	echo -e "${green}"
	}

	#bold text function, not actually invoked in end - too difficult to pick out text variable
	# could get this to work as a function, done manually below, as required
	function highlight
	{
	echo -e "\e[1;32m {$text} \n${endColor}"
	}
	#_____________________________________________________________________________________________
# THE REMOVE CUSTOMER PAGE FUNCTIONS

# Confirms the logged-in User in the 'Add Customer' page
	echo -e "${green}Logged-in User: \e[1;32m $USER \n${endColor}" 
	# \e[1;32;40m : \e[attribute code (bold);text color code (green);background color code(black)

	#to display the Menu framing lines, calls the highlight function defined before
	frame Menu 
	# Menu title in bold text
	echo -e "\e[1;32m          ***REMOVE CUSTOMER DETAILS**                \n${endColor}"
	# calls cyan text function
	gen_font 
	echo "Please enter the Name of the Customer you wish to remove:"
	echo "(in the format firstname surname)" 
	echo
	frame Menu 

# COMPRISES SEVERAL STEPS BEDFORE REMOVING CUSTOMER
# 1. OUTER IF-ELSE statement - while the searchterm entered is correct, do the following:
	# 2. if more than one match exists ==>Use alias to narrow search to one unique match
	# 3. Elseif  only 1 match
	# 5. Else if no match exists, search again or exit menu etc..
	echo
	read -p 'Search term: ' searchterm
	echo "You entered the searchterm '$searchterm'. Is this correct (y/n)?"
	read response 
	# case insensitive read -i can work here, but cannot implement for the if-else statements
	match=`grep -i $searchterm	CustDetails.tsv` #case insenstive match
	count_match=`grep -ci $searchterm	CustDetails.tsv` # the number of case-insenstive matches

# Outer Else-If statement for the various options where the search term correct
	if [ $response == "y" ] 
			then

	# Else-If, OPTION 1: if more than customer match:
	# Uses alias to narrow down customer selection
	if [ $count_match -ge 2 ] #greater than or equal to 2
	then 
		frame
		echo "$count_match no. matches exist for this customer name, as follows:"
		echo
		echo "$match'\n'" # to separate the results on new line
		echo
		frame
		echo "Please enter the alias of your selected customer:" # enter unique alias ID
		echo
		read -p 'Alias: ' alias
		aliasmatch=`grep -i $alias	CustDetails.tsv` #case insenstive match
		echo
		echo "The following (unique) customer matches your query :"
		echo
		echo $aliasmatch
		echo
		echo "Please confirm whether you wish to remove the customer (y/n):"
		echo
		frame
		read response
		# Inner If-else statment to confirm customer removal
		# If user confirms that they wish to remove this customer
		if [ $response == "y" ] 
		then
			echo
			echo Removed customer entry "$alias"
			echo
			frame
			grep -v "$alias" CustDetails.tsv > tmp/Details.tsv
			#selects the lines not matching the submitted details/and sends to a temporary Details.tsv file
			mv tmp/Details.tsv CustDetails.tsv # moves or renames the (source) temporary file to (destination) file;;
			# ie only ultimately removes those lines matching the submitted details
			#echo "Customer removed";;
		else
			# inner Else-If statement, if customer changes mind and does not wish to remove details
			echo
			echo Customer details retained
			echo
			frame
		fi

	# Else-If statement, OPTION 2: If no customer match to the query	exist
	# hit this condition first as next condition le2 is also valid (0 less than 2)
	elif [ $count_match -le 0  ] # if no match exists (less than /equal to 0)
	then	
		echo "No customer name matches your query"
		echo
		frame

	# Else-If statement, OPTION 3: if only one customer match:
	# if count less than orr equal 2 and greater than 1, ie one only
	elif [ $count_match -le 2 ] # if only one match exists
	then	
		echo
		echo "The following customer matches your query :"
		echo
		frame
		echo
		echo $match
		echo
		frame
		echo
		echo "Please confirm that you wish to remove the customer (y/n):"
		echo
		read response
		# If user confirms that they wish to remove this customer
		if [ $response == "y" ] 
		then
			echo "Removed customer entry '$searchterm'"
			echo
			frame
			grep -v "$searchterm" CustDetails.tsv > tmp/Details.tsv
			#selects the lines not matching the submitted details/and sends to a temporary Details.tsv file
			mv tmp/Details.tsv CustDetails.tsv # moves or renames the (source) temporary file to (destination) file;;
			# ie only ultimately removes those lines matching the submitted details
			#echo "Customer removed";;

		# inner Else-If statement, if customer changes mind and does not wish to remove details
			else
			echo Customer details retained
			echo
		frame
		fi
	# Else-If statement, OPTION 4: To return to mensu selection again
	else
		echo "Return to the Menu selection"
		echo
		frame
	fi

	#Outer If-Else statement if customer not happy with search details entered in the first instance, 
	# begin search again
	else
		echo "Incorrect details entered. Please try again"
		echo
		frame
	fi
	
	echo		
	echo Please choose your next option:
	echo
	echo -e "${green} 1. ${endColor}" "${magenta}Remove Customer${endColor}" 
	echo -e "${green} 2. ${endColor}" "${magenta}Return to the Main Menu${endColor}" 
	echo -e "${green} 3. ${endColor}" "${magenta}Exit${endColor}" 
	echo
	frame
	sleep 2 # wait 2 seconds

# calls cyan text function
	gen_font 
	echo Enter Number now
	read selection

# ---------------------------------------------------------------------------------------------------
	# NAVIGATION INSTRUCTIONS TO THE SHELL

	# uses case statement for selection
	# note 'exit' command after each navigation option 
	# this ensures this script instance closes after exit 
	case $selection in 
	#1) end=0;; will loop automatically to beginning this file
	2) ./Menu.sh; exit;; # back to Menu
	3) end=1;; # exits the file
	#*) echo invalid input; exit;; # triggers the user to input another selection
	# causes error with selection of 1 where 1) not explicitly stated and 
	# 1) is instead interpreted as a wild card
esac

#end of page While Loop
done


 
